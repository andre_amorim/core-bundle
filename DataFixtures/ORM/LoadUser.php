<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Defesa\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Defesa\CoreBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUser implements FixtureInterface, ContainerAwareInterface{
    
    private $container;
    
    public function load(ObjectManager $manager) {
        
        $user = new User();
        $user->setUsername("user");
        $user->setPassword($this->encodePassword($user, "123456"));
        $user->setIsActive(false);
        $user->setEmpresa(1);
        $manager->persist($user);
        
        $admin = new User();
        $admin->setUsername("admin");
        $admin->setRoles(array("ROLE_ADMIN"));
        $admin->setIsActive(true);
        $admin->setPassword($this->encodePassword($admin, "123456"));
        $admin->setEmpresa(1);
        $manager->persist($admin);
        
        $manager->flush();
    }

    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }    
    
    private function encodePassword($user, $plainPassword){
        $encoder = $this->container->get("security.encoder_factory")
                        ->getEncoder($user);
        return $encoder->encodePassword($plainPassword, $user->getSalt());
    }
}