<?php

namespace Defesa\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('password','password')
            ->add('salt')
            ->add('empresa','entity', array(
                'class'     => 'CoreBundle:Empresa',
                'property'  => 'nome'
            ))
            ->add('roles', 'choice',
                    array('choices'=>
                    array(
                        'ROLE_ADMIN' => 'Administrador', 
                        'ROLE_USER' => 'Usuario'),
                        'expanded'=> true,
                        'multiple'=> true))
            ->add('isActive')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Defesa\CoreBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ra_corebundle_user';
    }
}
