<?php

namespace Defesa\CoreBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;

/**
 * Serviço de paginação de registros.
 *
 * @package Defesa\CoreBundle\Service
 * @author Andre Amorim <andre.amorim@ctis.com.br>
 */
class PaginationService
{

    private $em;
    private $knpPagination;

    public function __construct(EntityManager $em, $knpPagination)
    {
        $this->em = $em;
        $this->knpPagination = $knpPagination;
    }

    /**
     * @param Request $request
     * @param $addressRepository
     * @param array $columns
     * @param string $filterColum
     * @param string $filterValue
     * @param array $filterArray
     * @param string $filterWhere
     * @return mixed
     */
    public function build(Request $request, $addressRepository, array $columns, $filterColum="", $filterValue="", $filterArray=[], $filterWhere="")
    {
        $range = $request->get('range');
        $search = $request->get('busca');
        $page = $request->get('page', 1);
        if ($range == "") {
            $range = 10;
        }
        $query = $this->createQuery($addressRepository, $search, $columns, $filterColum, $filterValue, $filterArray, $filterWhere);
        return $this->createPagination($range, $page, $query);
    }

    /**
     * @param Request $request
     * @param $query
     */
    public function manualBuild(Request $request, $query)
    {
        $range = $request->get('range');
        $page = $request->get('page', 1);
        if ($range == "") {
            $range = 10;
        }
        return $this->createPagination($range, $page, $query);
    }

    /**
     * @param $repository
     * @param $search
     * @param array $columns
     * @param $filterColum
     * @param $filterValue
     * @param $filterArray
     * @param $filterWhere
     * @return mixed
     */
    private function createQuery($repository, $search, array $columns, $filterColum, $filterValue, $filterArray, $filterWhere)
    {
        $rp = $this->em->getRepository($repository);
        if ($search == "") {
            $query = $this->createQueryBasic($rp, $filterColum, $filterValue, $filterArray, $filterWhere);
        } else {
            $query = $this->createQuerySearch($rp, $search, $columns, $filterColum, $filterValue, $filterArray, $filterWhere);
        }
        return $query;
    }

    /**
     * @param $rp
     * @param $search
     * @param $columns
     * @param $filterColumn
     * @param $filterValue
     * @param $filterArray
     * @param $filterWhere
     * @return mixed
     */
    private function createQuerySearch($rp, $search, $columns, $filterColumn, $filterValue, $filterArray, $filterWhere){
        $query = $rp->createQueryBuilder('a');

        # filtra por uma coluna unica
        if($filterColumn != ""){
            $query->andWhere('a.'.$filterColumn.'=:value');
            $query->setParameter('value',$filterValue);
        }
        # filtra por um array de parametros
        foreach ($filterArray as $itemFilter => $itemFilterValue) {
            $query->andWhere('a.'.$itemFilter.'=:value');
            $query->setParameter('value',$itemFilterValue);
        }
        # varre as colunas parametrizadas como busca
        foreach ($columns as $col) {
            $query->orWhere('lower(a.' . $col . ') LIKE :' . $col . '');
            $query->setParameter($col, '%' . strtolower($search) . '%');
        }
        if ($filterWhere != "") {
            $query->andWhere('a.'.$filterWhere);
        }
        return $query->getQuery();
    }

    /**
     * @param $rp
     * @param $filterColumn
     * @param $filterValue
     * @param $filterArray
     * @param $filterWhere
     * @return mixed
     */
    private function createQueryBasic($rp, $filterColumn, $filterValue, $filterArray, $filterWhere){
        if($filterColumn == ""){
            return $rp->createQueryBuilder('a')->getQuery();
        }else{
            $query = $rp->createQueryBuilder('a');
            $query->orWhere('a.'.$filterColumn.' = :'.$filterColumn.'');
            $query->setParameter($filterColumn, $filterValue);
            # filtra por um array de parametros
            foreach ($filterArray as $itemFilter => $itemFilterValue) {
                $query->andWhere('a.'.$itemFilter.'=:value');
                $query->setParameter('value',$itemFilterValue);
            }
            if ($filterWhere != "") {
                $query->andWhere('a.'.$filterWhere);
            }
            return $query->getQuery();
        }

    }

    /**
     * Cria o objeto de paginação com os dados carregados
     * @param $range Alcance de paginas, por exemplo de 0 à 10
     * @param $page Página atual
     * @param $query Consulta padrão do doctrine
     * @return $pagination Obejto com o resultado da paginação
     */
    private function createPagination($range, $page, $query)
    {
        $pagination = $this->knpPagination->paginate(
            $query,
            $page,
            $range
        );
        return $pagination;
    }

}
