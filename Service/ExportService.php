<?php
namespace Defesa\CoreBundle\Service;

use PHPExcel_Worksheet_Drawing;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Classe Especialista em exportar arquivos no formato PDF e EXCEL
 *
 * @author Andre Amorim
 */
class ExportService
{
    private $phpExcel;
    private $em;
    private $templating;
    private $container;


    /**
     * @param $phpExcel
     * @param $em
     * @param EngineInterface $templating
     * @param Container $container
     */
    public function __construct($phpExcel, $em, EngineInterface $templating, Container $container)
    {
        $this->phpExcel = $phpExcel;
        $this->em = $em;
        $this->templating = $templating;
        $this->container = $container;
    }

    /**
     * Método reponsável por gerar o arquivo excel
     *
     * @param String $title nome da guia é titulo do relatorio
     * @param String $institutionalSubscription texto localizado abaixo da logo
     * @param Object $entitys entidade no qual necessita das informações no relátorio
     * @param Array $ignoredColumns colunas a serem Ignoradas no relatório
     * @return Object
     */
    public function exportExcel($title, $institutionalSubscription, $entitys, $ignoredColumns = array())
    {
        $method = $this->filterNameMethod(get_class_methods($entitys[0]), $ignoredColumns);
        $alphas = range('A', 'Z');
        $phpExcelObject = $this->phpExcel->createPHPExcelObject();
        $phpExcelObject
            ->getProperties()
            ->setCreator("sistama.defesa")
            ->setLastModifiedBy("sistama defesa")
            ->setTitle(substr($title, 0, 31))
            ->setSubject('Relatório gerado por Ministério da Defesa');
        $styleArray = array('font' => array('bold' => true, 'color' => array('rgb' => '000000'), 'size' => 15));
        $phpExcelObject = $this->mountHeaderExcel($phpExcelObject, $institutionalSubscription, $title, $styleArray);
        $phpExcelObject = $this->mountTitleColumsExcel($phpExcelObject, $method, $styleArray, $alphas);
        $phpExcelObject = $this->mountValueColumnsExcel($phpExcelObject, $alphas, $entitys, $method, $styleArray);
        $phpExcelObject->getActiveSheet()->setTitle(substr($title, 0, 31));
        $phpExcelObject->setActiveSheetIndex(0);
        $response = $this->phpExcel->createStreamedResponse($this->phpExcel->createWriter($phpExcelObject, 'Excel5'));
        $response->headers->set('Content-Type', 'application/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename=' . $title . '.xls');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        return $response;
    }

    /**
     * Método reponsável por montar o cabeçalho do relatório com a logo e o texto que localiza abaixo da logo
     *
     * @param Object $phpExcelObject objeto do phpExcel
     * @param String $institutionalSubscription texto que localiza abaixo da logo.
     * @param String $title nome da guia é titulo do relatorio
     * @param String $styleArray estilo para o titulo do relatório
     * @return Object
     */
    private function mountHeaderExcel($phpExcelObject, $institutionalSubscription, $title, $styleArray)
    {
//        $institutionalSubscription=str_replace('<br/>','',$institutionalSubscription);
//        $wrapTex = $phpExcelObject->getActiveSheet();
//        $wrapTex->getStyle('A10')->getAlignment()->setWrapText(true);
//        $wrapTex->getRowDimension('1')->setRowHeight(-1);
//        $phpExcelObject->getActiveSheet()->getStyle('A10:G1' . round((strlen($institutionalSubscription) / 40) - 1))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
//        $phpExcelObject->getActiveSheet()->getStyle('A10:G1' . round((strlen($institutionalSubscription) / 40) - 1))->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
//        $phpExcelObject->setActiveSheetIndex(0)->mergeCells('A10:G1' . round((strlen($institutionalSubscription) / 40) - 1));
//        $phpExcelObject->setActiveSheetIndex(0)->setCellValue('A10', $institutionalSubscription);
//        $objDrawing = new PHPExcel_Worksheet_Drawing;
//        $objDrawing->setPath('images/logo-relatorio-pequena-novo.png')
//            ->setCoordinates('D2')
//            ->setHeight(140)
//            ->setWidth(105)
//            ->setWorksheet($phpExcelObject->getActiveSheet());
//        $phpExcelObject->setActiveSheetIndex(0)->mergeCells('D1:E8');
//        $phpExcelObject->getActiveSheet()->getStyle('D1:E8')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $phpExcelObject->setActiveSheetIndex(0)->mergeCells('A1:H1');
        $phpExcelObject->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
        $phpExcelObject->setActiveSheetIndex(0)->setCellValue('A1', $title);
        $phpExcelObject->getActiveSheet()->getStyle('A1:G1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        return $phpExcelObject;
    }

    /**
     * Método reponsável montar os titulos da coluna do relatório
     *
     * @param Object $phpExcelObject objeto do phpExcel
     * @param Array $classMetodos todos os metodos que será mostrado no relatório
     * @param String $styleArray estilo para o titulo da coluna do relatório
     * @param Array $alphas array com as letras que será usada para mapeamento no excel
     * @return Object
     */
    private function mountTitleColumsExcel($phpExcelObject, $classMetodos, $styleArray, $alphas)
    {
        $header = '';
        foreach ($classMetodos as $key => $colum) {
            $header .= strtolower(str_replace('get', '', $colum)) . ' ';
        }
        $titleColums = array_map('ucfirst', array_filter(explode(' ', $header)));
        foreach ($titleColums as $key2 => $colum) {
            $phpExcelObject->setActiveSheetIndex(0)->setCellValue($alphas[$key2] . '3', $colum);
            $phpExcelObject->getActiveSheet()->getStyle($alphas[$key2] . '3')->applyFromArray($styleArray);
        }
        return $phpExcelObject;
    }

    /**
     *  Método reponsável montar os valores da coluna do relatório
     *
     * @param Object $phpExcelObject objeto do phpExcel
     * @param Array $alphas array com as letras que será usada para mapeamento no excel
     * @param Object $entitys entidade no qual precisa gerar os dados do relatorio
     * @param String $classMetodos colunas que será mostrado no relatório
     * @return Object
     */
    private function mountValueColumnsExcel($phpExcelObject, $alphas, $entitys, $classMetodos)
    {
        for ($i = 0; $i < count($entitys); $i++) {
            foreach ($classMetodos as $key2 => $colum) {
                $phpExcelObject->getActiveSheet()
                    ->getColumnDimension($alphas[$key2])
                    ->setAutoSize(true);
                if ($entitys[$i]->$colum() instanceof \DateTime) {
                    $entitys[$i]->$colum()->format('Y-m-d H:i:s');
                    $phpExcelObject->setActiveSheetIndex(0)->setCellValue($alphas[$key2] . $cellCont = $i + 4, $entitys[$i]->$colum()->format('d/m/Y'));
                } else {
                    $phpExcelObject->setActiveSheetIndex(0)->setCellValue($alphas[$key2] . $cellCont = $i + 4, $entitys[$i]->$colum());
                }
            }
        }
        return $phpExcelObject;
    }

    /**
     * Método reponsável por filtrar os nomes dos metodos que será necessário para geração do relatório
     *
     * @param Object $classMetodos todos os metodos da entidade
     * @param Array $ignoredColumns colunas a serem ignoradas do relatório
     * @return Array
     */
    public function filterNameMethod($classMetodos, $ignoredColumns = array())
    {
        foreach ($classMetodos as $key => $colum) {
            if (strripos($colum, 'id') === false && strripos($colum, 'set') === false) {
                if (!in_array(strtolower(str_replace('get', '', $colum)), array_map('strtolower', $ignoredColumns))) {
                    $method[] = $colum;
                }
            }
        }

        return $method;
    }

    //*******************************************************************************************************

    /**
     * @param $title
     * @param $institutionalSubscription
     * @param $entitys
     * @param array $ignoredColumns
     * @param string $fileName
     * @return Response
     */
    public function exportPdf($title, $institutionalSubscription, $entitys, $ignoredColumns = array(), $fileName = "arquivo")
    {

        $method = $this->filterNameMethod(get_class_methods($entitys[0]), $ignoredColumns);

        $objPdfTitle = $this->mountTitleColumsPdf($method);
        $objPdfValue = $this->mountValueColumnsPdf($entitys, $method);

        $html = $this->container->get('templating')->render('RelatorioBundle:Export:pdf.html.twig', array(
            'objPdfTitle' => $objPdfTitle,
            'objPdfValue' => $objPdfValue,
            'title' => $title,
            'institutionalSubscription' => $institutionalSubscription));

        return new Response(
            $this->container->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="'.$fileName.'.pdf"'
            )
        );
    }

    /**
     * @param $html
     * @param string $fileName
     * @return Response
     */
    public function exportHtmlToPdf($html, $fileName = "arquivo")
    {
        return new Response(
            $this->container->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="'.$fileName.'.pdf"'
            )
        );
    }

    /**
     * @param $html
     * @return Response
     */
    public function exportPdfWithoutTab($html)
    {
        $html = $this->container->get('templating')->render('RelatorioBundle:Export:pdfWithoutTab.html.twig', array(
                'html' => $html)
        );

        // @TODO: Finalizar a geração do pdf, esperando o Bruno terminar o ambiente.

        echo "<pre>";
        print_r($html);
        die;

        return new Response(
            $this->container->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="questionario.pdf"'
            )
        );
    }

    /**
     * Método reponsável montar os titulos da coluna do relatório
     * @param Array $classMetodos todos os metodos que será mostrado no relatório
     * @return Array
     */
    private function mountTitleColumsPdf($classMetodos)
    {
        $header = '';
        foreach ($classMetodos as $key => $colum) {
            $header .= strtolower(str_replace('get', '', $colum)) . ' ';
        }
        $titleColums = array_map('ucfirst', array_filter(explode(' ', $header)));

        return $titleColums;
    }

    /**
     *  Método reponsável montar os valores da coluna do relatório
     * @param Object $entitys entidade no qual precisa gerar os dados do relatorio
     * @param String $classMetodos colunas que será mostrado no relatório
     * @return Array
     */
    private function mountValueColumnsPdf($entitys, $classMetodos)
    {
        $objPdf = array();
        for ($i = 0; $i < count($entitys); $i++) {
            foreach ($classMetodos as $key => $colum) {
                if ($entitys[$i]->$colum() instanceof \DateTime) {
                    $entitys[$i]->$colum()->format('Y-m-d H:i:s');
                    $objPdf[] = $entitys[$i]->$colum()->format('d/m/Y');
                } else {
                    $objPdf[] = $entitys[$i]->$colum();
                }
            }
        }

        return $objPdf;
    }

}