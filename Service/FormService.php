<?php

namespace Defesa\CoreBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Serviço de criação de forms auxiliares para ações genericas.
 *
 * @package Defesa\CoreBundle\Service
 * @author Andre Amorim <andre.amorim@ctis.com.br>
 */
class FormService
{

    private $em;
    private $formFactory;
    private $router;

    public function __construct(EntityManager $em, $formFactory, $router)
    {
        $this->em = $em;
        $this->formFactory = $formFactory;
        $this->router = $router;
    }

    /**
     * Cria um array de formularios de deleção para cada item da entidade
     * @param $addressRepository
     * @param $route rota de deleção da entidade
     * @return array lista de formularios de deleção
     */
    public function createDeleteForms($addressRepository, $route)
    {
        $entities = $this->em->getRepository($addressRepository)->findAll();
        foreach ($entities as $entity) {
            $deleteForms[$entity->getId()] = $this->createForm($entity, $route, 'DELETE')->createView();
        }
        if (!isset($deleteForms)) {
            $deleteForms = array();
        }
        return $deleteForms;
    }

    /**
     * Criar um formulário para manipulação de dados
     * @param $entity entidade do formulario a ser manipulada
     * @param $route rota da ação desejada
     * @param $method metodo da ação desejada
     * @param string $objType objeto do formulario formBuilder
     * @return mixed formulario montado para manipulação
     */
    public function createForm($entity, $route, $method, $objType = '')
    {
        $action = $this->router->generate($route, ['id' => $entity->getId()], UrlGeneratorInterface::ABSOLUTE_PATH);
        if ($method == 'DELETE') {
            $form = $this->formFactory->createBuilder('form', null, [])
                ->setAction($action)
                ->setMethod($method)
                ->getForm();
        } else {
            $form = $this->formFactory->create($objType, $entity, array(
                'action' => $action,
                'method' => $method
            ));
        }
        return $form;
    }

}
